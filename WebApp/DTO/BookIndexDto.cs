﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain;

namespace WebApp.DTO
{
    public class BookIndexDto
    {
        [Required] public Book Book { get; set; } = default!;
        public int CommentCount { get; set; }
        public string? LastComment { get; set; }
    }
}